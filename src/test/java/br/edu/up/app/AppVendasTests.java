package br.edu.up.app;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.edu.up.app.dominio.Cliente;
import br.edu.up.app.dominio.ProdutoVendas;
import br.edu.up.app.services.VendasService;
import br.edu.up.app.webservices.WsPessoas;
import br.edu.up.app.webservices.WsProdutos;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AppVendasTests {

	@Autowired
	VendasService sVendas;
	
	@Autowired
	public WsPessoas wsPessoas;
	
	@Autowired
	public WsProdutos wsProdutos;
	
	@Test
	public void testarInclusaoDeVenda() {
		
		//Cliente cliente = wsPessoas.buscarClientePorId(3L);
		Cliente cliente = wsPessoas.buscarClientePorNome("Pedro");
		ProdutoVendas produto = wsProdutos.buscarProdutoPorId(1L);
		sVendas.salvarVenda(cliente, produto);
	}
}
