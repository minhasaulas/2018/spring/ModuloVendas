package br.edu.up.app.webservices;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import br.edu.up.app.dominio.Cliente;

@Component
public class WsPessoas {
	
	private final RestTemplate rest = new RestTemplate();
    private final String srvPessoas = "http://localhost:8080/pessoas";
	
	public Cliente buscarClientePorId(Long idCliente) {
		Map<String, Long> params = new HashMap<>();
		params.put("id", idCliente);
		return rest.getForObject(srvPessoas + "/{id}?projection=cliente", Cliente.class, params);
	}
	
	public Cliente buscarClientePorNome(String nome) {
		Map<String, String> params = new HashMap<>();
		params.put("nome", nome);
		return rest.getForObject(srvPessoas + "/search/findByNome?nome={nome}&projection=cliente", Cliente.class, params);
	}
}
